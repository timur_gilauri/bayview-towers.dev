(function() {
  self.onconnect = function(e) {
    let port = e.ports[0];
    port.onmessage = function (e) {
      if (e.data.message === 'hello') {
        return 'Glad to read from you at another page!';
      }
    }
  }
})();