let worker = new Worker('./assets/templates/default/js/workers/worker.js');
let sharedWorker = new SharedWorker('./assets/templates/default/js/workers/shadow-worker.js');

window.onload = function () {
  
  worker.postMessage({message: 'hello'});
  
  worker.onmessage = function (e) {
    console.log(e.data);
  };
  
  sharedWorker.port.postMessage({message: 'hello'});
  
  sharedWorker.port.onmessage = function (e) {
    console.log(e.data);
  };
};