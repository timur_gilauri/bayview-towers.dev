import Vue from 'vue';

new Vue({
  components: {},
  el: '#towers',
  data: {
    activeTower: 0,
    activeFloor: 0,
    towers: []
  },
  computed: {
    activeTowerFloors() {
      return this.towers[this.activeTower].floors;
    },
    activeFloorSchema() {
      return this.activeTowerFloors[this.activeFloor].image;
    },
    towerClasses() {
      return this.towers.map((item, index) => {
        let classes = {};
        classes['tower-' + index] = true;
        classes['active-tower'] = this.activeTower === index;
        return classes;
      });
    }
  },
  created() {
    this.towers = window.Towers;
    $('#towers-init').remove();
  },
  methods: {
    changeActiveTower(tower) {
      this.activeTower = tower;
      this.activeFloor = 0;
    },
    changeActiveFloor(floor) {
      //console.log(floor);
      this.activeFloor = floor;
    }
  }
});
