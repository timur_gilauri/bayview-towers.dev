/*jslint browser: true*/
/*global $, jQuery, alert, console, ymaps, MutationObserver*/

/* Функция обозреватель событий и изменения DOM */

$.fn.observe = function (options, callableFunction) {
  'use strict';
  if (!$(this).length) {
    return false;
  }
  var $this = $(this),
    config = (!options || typeof options === 'function') ? {
      attributes: true,
      childList: true,
      characterData: true,
      attributeOldValue: true
    } : options,
    func = (typeof callableFunction === 'undefined' || typeof options === 'function') ? options : null,
    observer = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (func) {
          func.call($this, mutation);
        }
      });
    });
  return observer.observe($(this)[0], config);
};

$(function () {
  'use strict';
  
  $('#review__slider__lst').bxSlider({
    slideWidth: $('#review__slider__lst').width(),
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 1,
    pager: false,
    nextSelector: '.next-button',
    prevSelector: '.prev-button'
  });
  
  
  /************************ Scroll to element **************************/
  $(".scroll").click(function (e) {
    e.preventDefault();
    var id = $(this).attr('href') || $(this).data('target');
    $('html, body').animate({
      scrollTop: $(id).offset().top
    }, 1500);
  });
  
  /* размытие фона на форме */
  
  $('input[type="text"], textarea', '.block_blur_effect').focus(function () {
    $('.block_blur_effect').addClass('blur');
  });
  $('input[type="text"], textarea', '.block_blur_effect').blur(function () {
    $('.block_blur_effect').removeClass('blur');
  });
  
  
  /**
   *  Функция изменения высоты блоков
   *  Выбираем все элементы-комментарии,
   *  находим элемент с наибольше высотой и назначаем эту высоту остальным.
   *  Если ширина экрана меньше 1024px, то высоту блока задаем 'auto'
   *
   * @param {wrapperElem} jQuery Object - родительский элемент
   *
   * @param {elems} jQuery Array - элементы, которым задаем высоту
   *
   **/
  
  jQuery.fn.setMinHeight = function (heightType) {
    var elems = $(this),
      minHeight = $(elems[0]).height();
    elems.each(function () {
      try {
        if (!heightType) {
          if ($(this).height() > minHeight) {
            minHeight = $(this).height();
          }
          $(this).css('min-height', minHeight + 'px');
        } else {
          $(this).css('min-height', heightType);
        }
      } catch (e) {
        console.log(e);
      }
    });
    if (!heightType) {
      $(elems[0]).css('min-height', minHeight + 'px');
    } else {
      $(elems[0]).css('min-height', heightType);
    }
  };
  
  
  $(window).resize(function () {
    if ($(window).innerWidth() < 630) {
      $('.c_block_horizontal', '.c_review').setMinHeight('auto');
    } else {
      $('.c_block_horizontal', '.c_review').setMinHeight();
    }
    if ($(window).innerWidth() < 1008) {
      $('.centered-column', '#additional-services').setMinHeight('auto');
    } else {
      $('.centered-column', '#additional-services').setMinHeight();
    }
  });
  $(window).resize();
  
  /* СЛАЙДЕРЫ */
  
  
  $('.gallery__slider').bxSlider();
  $('.panoramas__slider').bxSlider({
    mode: 'fade',
    controls: false,
    onSliderLoad: function () {
      
      var currentSlide = $('.panoramas').find('.panoramas__slider__image:visible'),
        captionText = currentSlide.data('caption'),
        classToAdd = currentSlide.data('addClass'),
        classToRemove = currentSlide.data('removeClass');
      
      $('#panoramas__slider__caption').text(captionText);
      $(".panoramas__slider__caption__wrapper").removeClass(classToRemove).addClass(classToAdd);
    },
    
    onSlideBefore: function ($slideElement, oldIndex, newIndex) {
      
      var nextSlide = $slideElement,
        captionText = nextSlide.data('caption'),
        classToAdd = nextSlide.data('addClass'),
        classToRemove = nextSlide.data('removeClass');
      
      $('#panoramas__slider__caption').text(captionText);
      $(".panoramas__slider__caption__wrapper").removeClass(classToRemove).addClass(classToAdd);
    }
  });
  var offices_slider = $('.offices__slider').bxSlider({
    onSlideBefore: function ($slideElement, oldIndex, newIndex) {
      if ((newIndex < oldIndex && !(newIndex === 0 && oldIndex === (offices_slider.getSlideCount() - 1))) || (oldIndex === 0 && newIndex === (offices_slider.getSlideCount() - 1))) { // next
        $('.office_' + (newIndex + 1)).addClass('office_left');
      } else { // prev
        $('.office_' + (newIndex + 1)).addClass('office_right');
      }
    },
    onSlideAfter: function ($slideElement) {
      $slideElement.removeClass('office_right').removeClass('office_left');
    }
  });
  
  /* Конференц-залы */
  
  
  /* ТАБЫ */
  
  $(function () {
    
    function setParams(params) {
      for (var param in params) {
        if (params.hasOwnProperty(param)) {
          if (param === 'confSchema') {
            $('.' + param).attr('src', params[param]);
          } else if (param === 'image') {
            $('#conference').css('background-image', 'url(' + params[param] + ')');
          } else {
            (param === 'confNumber') && $('.confNumberSelect').val(params[param]);
            $('.' + param).text(params[param]);
          }
        }
      }
    }
    
    // При загрузке
    
    var initialParams = $('.tabs').find('.active-tab').data();
    
    setParams(initialParams);
    
    // Клики по табам
    
    $('.tabs-title').click(function () {
      if ($(this).is('.is-active')) {
        return false;
      }
      $('.tabs').find('.active-tab').removeClass('active-tab');
      $(this).addClass('active-tab');
      
      var params = $(this).data();
      setParams(params);
      
    });
    
    /* Модальные окна */
    
    $('.open-button').click(function () {
      var currentlyOpened = $('.modal__overlay.opened');
      currentlyOpened.length && currentlyOpened.removeClass('opened');
      
      $('#' + $(this).data('open')).addClass('opened');
      $('html, body').animate({
        scrollTop: $('#conference').offset().top - $('.sticky__menu').height()
      }, 300);
    });
    $('.close-button').click(function () {
      $('#' + $(this).data('close')).removeClass('opened');
    });
  });
  
  
  /* Проверка формы перед отправкой, т.к. recaptcha и ajaxForm перехватывают событие submit и reportValidity() не срабатывает */
  
  $('form').submit(function () {
    if (!$(this).closest('form')[0].reportValidity()) {
      return false;
    }
  });
  
  /* Верхнее прилипшее меню */
  
  $(function () {
    
    var body = $('body'),
      menu = $('.sticky__menu'),
      menuWrapper = $('.sticky__menu__wrapper'),
      menuInner = $('.sticky__menu__inner'),
      closeButton = $('.sticky__menu__close'),
      stickyMenuItem = $('.sticky__menu__lst__i, .sticky__menu__title'),
      collapsedClass = 'sticky__menu__collapsed',
      forMenuClass = 'for_menu';
    
    closeButton.click(function () {
      body.hasClass('for_menu') ? body.removeClass(forMenuClass) : body.addClass(forMenuClass);
      if (menu.hasClass(collapsedClass)) {
        menu.removeClass(collapsedClass);
      } else {
        menu.addClass(collapsedClass);
      }
    });
    
    stickyMenuItem.click(function () {
      !menu.hasClass(collapsedClass) && menu.addClass(collapsedClass);
      body.hasClass(forMenuClass) && body.removeClass(forMenuClass);
    });
    
    function checkForOverflow() {
      //console.log('checking for overflow');
      var winH = $(window).height(),
        menuWrapperHeight = menuWrapper.height();
      if (menuWrapperHeight < winH) {
        menuWrapper.css('overflow', 'auto');
      }
    }
    
    checkForOverflow();
    
    $(window).resize(checkForOverflow)
    
  });
  /* Паралакс в верхнем блоке */
  
  $(document).on('scroll', function () {
    var scTop = $(document).scrollTop(),
      headerH = $('#header').height(),
      menuOpacity = parseInt($('#sticky__menu').css('opacity'), 10);
    $('.header__inner__head').css({
      'top': (scTop / -4) + "px"
    });
    $('.header__inner__background').css({
      'top': (scTop / -1.5) + "px"
    });
    if (scTop > headerH && menuOpacity === 0) {
      $('#sticky__menu').css({
        'opacity': 1
      });
    } else if (scTop < headerH && menuOpacity === 1) {
      $('#sticky__menu').css({
        'opacity': 0
      });
    }
  });
  $('.header__inner__head').css({
    'top': ($(document).scrollTop() / -4),
    'opacity': 1
  });
  if ($('.header__inner__background').height() < $('#header').height()) {
    $('.header__inner__background').css({
      'height': ($('#header').height())
    });
  }
  
  
  /* Указатель меню в заглавной секции */
  
  $('.menu__header__i__a').on('mouseenter', function () {
    var item_w = $(this).width(),
      item_h = $(this).height(),
      item_offset = $(this).offset(),
      item_parent_offset = $('.menu__header').first().offset();
    $('.menu__header__pointer').css({
      "width": (item_w + 40) + 'px',
      "height": (item_h + 20) + 'px',
      "top": (item_offset.top - item_parent_offset.top) + "px",
      "left": (item_offset.left - item_parent_offset.left) + "px"
    });
  }).on('mouseleave', function () {
    $('.menu__header__pointer').css({
      "width": '0px',
      "left": '50%'
    });
  })
  
  
  function isRequiredInputsFilled($form) {
    var filled = true;
    $form.find('input[required]').each(function () {
      if ($(this).val().length === 0) {
        filled = false;
        return false;
      }
    });
    return filled;
  }
  
  $.fn.isRequiredInputsFilled = function () {
    return isRequiredInputsFilled.call($(this), $(this));
  };
  
  function formValidation(formSelector, agreementSelector, inputSelector, buttonSelector, errorClass) {
    var agreement = $(agreementSelector, formSelector),
      inputs = $(inputSelector, formSelector),
      button = $(buttonSelector, formSelector);
    
    
    agreement.change(function () {
      //console.log($(this).is(':checked'));
      if ($(this).is(':checked')) {
        inputs.each(function () {
          $(this).val().length === 0 && $(this).addClass(errorClass);
        });
        button.attr('disabled', $('.' + errorClass, formSelector).length > 0);
      } else {
        button.attr('disabled', true);
      }
    });
    
    inputs.each(function () {
      $(this).on('input', function () {
        //console.log($(this).val());
        if ($(this).val().length > 0) {
          $(this).hasClass(errorClass) && $(this).removeClass(errorClass);
        } else {
          !$(this).hasClass(errorClass) && $(this).addClass(errorClass);
        }
        button.attr('disabled', !(($('.' + errorClass, formSelector).length === 0) && agreement.is(':checked')));
      });
    });
  }
  
  formValidation('#requestForm', '.personal-info-agreement', '[required]', '.submit_button', 'error');
  formValidation('#leasingForm', '.personal-info-agreement', '[required]', '.submit_button', 'error');
  
  $('.submit_button').each(function () {
    
    $(this).attr('disabled', !$(this).closest('.form').isRequiredInputsFilled());
    
    $(this).observe(function (mutation) {
      if (mutation.attributeName === 'disabled') {
        //console.log(mutation);
        if (!$(this).attr('disabled') && !$(this).closest('.form').isRequiredInputsFilled()) {
          $(this).attr('disabled', true);
          return false;
        }
      }
    });
  });
  
  
});